#!/usr/bin/env bash
echo -e $(curl -s https://www.viedemerde.fr/aleatoire | grep -A 1 -m 1 'class="block text-blue-500 dark:text-white my-4' | awk '/Aujourd&#39;hui|VDM/ {match($0, /Aujourd&#39;hui|VDM/); print substr($0, RSTART + RLENGTH)}' | sed 's/^/Aujourd&#39;hui/' | sed "s/&#39;/'/g; s/#39;/'/g" | sed 's/^/\\e[1;31m/; s/$/\\e[0m/')
